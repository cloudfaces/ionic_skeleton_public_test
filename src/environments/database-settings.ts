export const databaseSettings = {
  firebase: {
    apiKey: 'AIzaSyAQ7YBvnm9aAggkap0T51Tt1RadZf1D4fI',
    authDomain: 'angular-skeleton-19f75.firebaseapp.com',
    databaseURL: 'https://angular-skeleton-19f75.firebaseio.com',
    projectId: 'angular-skeleton-19f75',
    storageBucket: 'angular-skeleton-19f75.appspot.com',
    messagingSenderId: '44736454867',
    appId: '1:44736454867:web:0c0a9198b42497b3e47426',
    measurementId: 'G-Y45B8P4RP7',
  },

  directus: {
    projectUrl: 'https://directus.cloudfaces.com',
    projectId: 'skeleton-ionic-app',
  },
};
