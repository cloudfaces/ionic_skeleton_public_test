import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'systemLanguages',
})
export class SystemLanguagesPipe implements PipeTransform {
  transform(systemLanguage: any[], moduleId: string, key: string): string {
    let result = '';

    if (systemLanguage) {
      systemLanguage.filter((sl) => {
        if (sl.module_uid === moduleId && sl.key === key) {
          result = sl.translations.find((obj) => obj.language === 'en').text;
        }
      });
    }

    return result;
  }
}
