import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './services/auth-guard.service';
import { SideMenuComponent } from './components/side-menu/side-menu.component';

const routes: Routes = [
  {
    path: '',
    // redirectTo: 'list-detail/KeyFigures',
    // redirectTo: 'login',
    redirectTo: 'side-menu',
    pathMatch: 'full',
  },
  {
    path: 'side-menu',
    component: SideMenuComponent,
  },
  {
    path: 'login',
    loadChildren: () =>
      import('./modules/login/login.module').then((m) => m.LoginPageModule),
  },
  {
    path: 'inbox/:id',
    loadChildren: () =>
      import('./modules/inbox/inbox.module').then((m) => m.InboxModule),
    // canActivate: [AuthGuard],
  },
  {
    path: 'list-detail/:id',
    loadChildren: () =>
      import('./modules/list-detail/list-detail.module').then(
        (m) => m.ListDetailModule
      ),
    // canActivate: [AuthGuard],
  },
  {
    path: 'list-detail-logos/:id',
    loadChildren: () =>
      import('./modules/list-detail-logos/list-detail-logos.module').then(
        (m) => m.ListDetailLogosModule
      ),
    // canActivate: [AuthGuard],
  },
  {
    path: 'category-details/:id',
    loadChildren: () =>
      import('./modules/category-details/category-details.module').then(
        (m) => m.CategoryDetailsModule
      ),
    // canActivate: [AuthGuard],
  },
  {
    path: 'simple-list/:id',
    loadChildren: () =>
      import('./modules/simple-list/simple-list.module').then(
        (m) => m.SimpleListModule
      ),
    // canActivate: [AuthGuard],
  },
  {
    path: 'notifications/:id',
    loadChildren: () =>
      import('./modules/notifications/notifications.module').then(
        (m) => m.NotificationsModule
      ),
    // canActivate: [AuthGuard],
  },
  {
    path: 'feedback/:id',
    loadChildren: () =>
      import('./modules/feedback/feedback.module').then(
        (m) => m.FeedbackModule
      ),
    // canActivate: [AuthGuard],
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
