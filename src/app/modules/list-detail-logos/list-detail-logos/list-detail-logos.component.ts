import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { ListDetailLogosService } from '../list-detail-logos-service';
import { LoadingController } from '@ionic/angular';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-list-detail-logos',
  templateUrl: './list-detail-logos.component.html',
  styleUrls: ['./list-detail-logos.component.scss'],
})
export class ListDetailLogosComponent implements OnInit {
  contentList = new BehaviorSubject([]);

  navigationExtras: NavigationExtras = {};

  path = '';
  routeId = '';

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private listDetailLogosService: ListDetailLogosService,
    private loadingCtrl: LoadingController
  ) {}

  async ngOnInit() {
    this.path = window.location.pathname;
    console.log('Path Inbox ', this.path);

    const loader = await this.loadingCtrl.create({
      message: 'Loading...',
    });

    await loader.present();

    this.routeId = this.activatedRoute.snapshot.paramMap.get('id');

    this.listDetailLogosService.initializeData(this.routeId).then((content) => {
      loader.dismiss();
      this.contentList.next(content);
    });
  }

  openItemDetail(content) {
    this.navigationExtras.state = {
      url: this.path,
      routeId: this.routeId,
      ...content,
    };

    this.router.navigate(
      [`${this.path}/list-item-detail-logos`],
      this.navigationExtras
    );
  }
}
