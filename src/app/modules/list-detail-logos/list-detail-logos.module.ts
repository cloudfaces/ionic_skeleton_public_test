import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListDetailLogosComponent } from './list-detail-logos/list-detail-logos.component';
import { ListItemDetailLogosComponent } from './list-item-detail-logos/list-item-detail-logos.component';
import { RouterModule } from '@angular/router';
import { SystemLanguagesPipe } from 'src/app/pipes/system-languages.pipe';
import { HeaderComponent } from 'src/app/components/header/header.component';

const routes = [
  {
    path: '',
    component: ListDetailLogosComponent,
  },
  {
    path: 'list-item-detail-logos',
    component: ListItemDetailLogosComponent,
  },
];

@NgModule({
  declarations: [
    ListDetailLogosComponent,
    ListItemDetailLogosComponent,
    SystemLanguagesPipe,
    HeaderComponent,
  ],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [
    ListDetailLogosComponent,
    ListItemDetailLogosComponent,
    SystemLanguagesPipe,
    HeaderComponent,
  ],
})
export class ListDetailLogosModule {}
