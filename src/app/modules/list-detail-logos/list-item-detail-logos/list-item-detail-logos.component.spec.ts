import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ListItemDetailLogosComponent } from './list-item-detail-logos.component';

describe('ListItemDetailLogosComponent', () => {
  let component: ListItemDetailLogosComponent;
  let fixture: ComponentFixture<ListItemDetailLogosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListItemDetailLogosComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ListItemDetailLogosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
