import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ListDetailLogosService } from '../list-detail-logos-service';

@Component({
  selector: 'app-list-item-detail-logos',
  templateUrl: './list-item-detail-logos.component.html',
  styleUrls: ['./list-item-detail-logos.component.scss'],
})
export class ListItemDetailLogosComponent implements OnInit {
  contentObject = {};

  routeInfo = {
    url: '',
    direction: 'back',
  };

  constructor(
    private router: Router,
    private listDetailLogosService: ListDetailLogosService
  ) {}

  ngOnInit() {
    this.contentObject = this.router.getCurrentNavigation().extras.state;
    console.log('Value of Object in List Item detail', this.contentObject);

    if (this.contentObject) {
      this.routeInfo.url = JSON.parse(JSON.stringify(this.contentObject)).url;
    }
  }

  isEntrepreneurAvailable(contentObject) {
    return (
      contentObject?.person_url ||
      contentObject?.person_name ||
      contentObject?.person_image?.data?.thumbnails
    );
  }

  isFirstCompanyAvailable(contentObject) {
    return (
      contentObject?.expertise ||
      contentObject?.read_more_link ||
      contentObject?.person_cv?.data?.full_url
    );
  }

  isSecondCompanyAvailable(contentObject) {
    return (
      contentObject?.revenue ||
      contentObject?.servicing_plan ||
      contentObject?.comments ||
      contentObject?.image?.data?.full_url
    );
  }
}
