import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-list-item-detail',
  templateUrl: './list-item-detail.component.html',
  styleUrls: ['./list-item-detail.component.scss'],
})
export class ListItemDetailComponent implements OnInit {
  pathToParent = '';

  contentList: NavigationExtras = {};
  routeInfo = {
    url: '',
    direction: 'back',
  };

  constructor(private router: Router) {}

  ngOnInit() {
    this.contentList = this.router.getCurrentNavigation().extras.state;

    if (this.contentList) {
      this.routeInfo.url = JSON.parse(JSON.stringify(this.contentList)).url;
    }
  }
}
