import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListDetailComponent } from './list-detail/list-detail.component';
import { ListItemDetailComponent } from './list-item-detail/list-item-detail.component';
import { RouterModule } from '@angular/router';
import { SystemLanguagesPipe } from 'src/app/pipes/system-languages.pipe';
import { HeaderComponent } from 'src/app/components/header/header.component';

const routes = [
  {
    path: '',
    component: ListDetailComponent,
  },
  {
    path: 'list-item-detail',
    component: ListItemDetailComponent,
  },
];

@NgModule({
  declarations: [
    ListDetailComponent,
    ListItemDetailComponent,
    SystemLanguagesPipe,
    HeaderComponent,
  ],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [
    ListDetailComponent,
    ListItemDetailComponent,
    SystemLanguagesPipe,
    HeaderComponent,
  ],
})
export class ListDetailModule {}
