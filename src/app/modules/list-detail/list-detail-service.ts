import { Injectable } from '@angular/core';
import { SideMenuService } from 'src/app/services/side-menu.service';
import { AppDataService } from 'src/app/services/app-data.service';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ListDetailService {
  systemLanguage = new BehaviorSubject([]);

  constructor(
    private menuService: SideMenuService,
    private dataService: AppDataService
  ) {}

  async initializeData(routeId) {
    console.log('routeId', routeId);
    const databaseCollections = this.menuService.getdatabaseCollectoin(routeId);
    console.log('Database Collections', databaseCollections);

    const tempItems = await this.dataService.getData(
      databaseCollections.collection
    );

    console.log('List Detail Service ', tempItems);

    // get System Language Data from Data service
    this.systemLanguage.next(this.dataService.systemLanguages[0]);

    return [...tempItems.data];
  }

  getLayoutInfo(routeId: string): string {
    return this.menuService.getLayoutFromDatabase(routeId);
  }
}
