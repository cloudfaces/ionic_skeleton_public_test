import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { ListDetailService } from '../list-detail-service';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-list-detail',
  templateUrl: './list-detail.component.html',
  styleUrls: ['./list-detail.component.scss'],
})
export class ListDetailComponent implements OnInit {
  navigationExtras: NavigationExtras = {};
  path = '';
  routeId = '';

  layout = 'without-card-list';

  contentList = new BehaviorSubject([]);

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private listDetailsService: ListDetailService,
    private loadingCtrl: LoadingController
  ) {}

  async ngOnInit() {
    this.path = window.location.pathname;
    console.log('Path Inbox ', this.path);

    const loader = await this.loadingCtrl.create({
      message: 'Loading...',
    });

    await loader.present();

    this.routeId = this.activatedRoute.snapshot.paramMap.get('id');

    this.layout = this.listDetailsService.getLayoutInfo(this.routeId);
    console.log('Value of layout', this.layout);

    this.listDetailsService.initializeData(this.routeId).then((content) => {
      loader.dismiss();
      this.contentList.next(content);
    });
  }

  openDetail(content) {
    this.navigationExtras.state = {
      url: this.path,
      routeId: this.routeId,
      ...content,
    };

    this.router.navigate(
      [`${this.path}/list-item-detail`],
      this.navigationExtras
    );
  }
}
