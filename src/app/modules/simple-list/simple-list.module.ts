import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SimpleListComponent } from './simple-list/simple-list.component';
import { RouterModule } from '@angular/router';
import { SystemLanguagesPipe } from 'src/app/pipes/system-languages.pipe';
import { HeaderComponent } from 'src/app/components/header/header.component';

const routes = [
  {
    path: '',
    component: SimpleListComponent,
  },
];

@NgModule({
  declarations: [SimpleListComponent, SystemLanguagesPipe, HeaderComponent],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [SimpleListComponent, SystemLanguagesPipe, HeaderComponent],
})
export class SimpleListModule {}
