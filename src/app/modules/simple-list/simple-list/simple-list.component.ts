import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppDataService } from 'src/app/services/app-data.service';
import { SideMenuService } from 'src/app/services/side-menu.service';
import { BehaviorSubject } from 'rxjs';
import { LoadingController } from '@ionic/angular';
import { SimpleListService } from '../simple-list-service';

@Component({
  selector: 'app-simple-list',
  templateUrl: './simple-list.component.html',
  styleUrls: ['./simple-list.component.scss'],
})
export class SimpleListComponent implements OnInit {
  routeId = '';

  contentList = new BehaviorSubject([]);

  constructor(
    private activatedRoute: ActivatedRoute,
    private loadingCtrl: LoadingController,
    private simpleListService: SimpleListService
  ) {}

  async ngOnInit() {
    const path = window.location.pathname;
    console.log('Path Inbox ', path);

    const loader = await this.loadingCtrl.create({
      message: 'Loading...',
    });

    await loader.present();

    this.routeId = this.activatedRoute.snapshot.paramMap.get('id');

    this.simpleListService.initializeData(this.routeId).then((content) => {
      loader.dismiss();
      this.contentList.next(content);
    });
  }
}
