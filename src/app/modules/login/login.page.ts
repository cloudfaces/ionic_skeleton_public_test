import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthenticationService } from './../../services/authentication.service';
import { AppDataService } from 'src/app/services/app-data.service';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  myForm: FormGroup;

  loginError = '';

  systemLanguage = new BehaviorSubject([]);

  routeId = 'Login';

  constructor(
    private fb: FormBuilder,
    private authService: AuthenticationService,
    private dataService: AppDataService
  ) {
    console.log('Inside Login Contructor');
  }

  async ngOnInit() {
    this.myForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]],
    });

    // Init System Languages Data
    const systemLanguageSnap = await this.dataService.getSystemLanguagesData(
      'system_languages'
    );

    // get System Language Data from Data service
    this.systemLanguage.next(this.dataService.systemLanguages[0]);
  }

  // form: FormGroup
  loginUser(email: string, password: string) {
    // show loading
    this.dataService.presentLoader({
      message: 'Signing In...',
    });

    // const email = form.value.email;
    // const password = form.value.password;

    console.log('Email', email);
    console.log('password', password);

    this.authService
      .loginUser(email, password)
      .then(async (res: any) => {
        console.log('Response of server of Directus', res);
      })
      .catch((error) => {
        this.dataService.dismissLoader();
        this.loginError = error.message;
        console.log('error while loggin in the users');
        console.log(error);
      });
  }
}
