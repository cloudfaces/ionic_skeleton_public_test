import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FeedbackComponent } from './feedback/feedback.component';
import { RouterModule } from '@angular/router';
import { HeaderComponent } from 'src/app/components/header/header.component';
import { FormsModule } from '@angular/forms';

const routes = [
  {
    path: '',
    component: FeedbackComponent,
  },
];

@NgModule({
  declarations: [FeedbackComponent, HeaderComponent],
  imports: [CommonModule, FormsModule, RouterModule.forChild(routes)],
  exports: [FeedbackComponent, HeaderComponent],
})
export class FeedbackModule {}
