import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { BehaviorSubject } from 'rxjs';
import { FeedbackService } from '../feedback-service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.scss'],
})
export class FeedbackComponent implements OnInit {
  routeId = '';
  contentList = new BehaviorSubject([]);
  feedback = '';

  constructor(
    private loadingCtrl: LoadingController,
    private feedbackService: FeedbackService,
    private activatedRoute: ActivatedRoute
  ) {}

  async ngOnInit() {
    const path = window.location.pathname;
    console.log('Path Feedback ', path);

    const loader = await this.loadingCtrl.create({
      message: 'Loading...',
    });

    await loader.present();

    this.routeId = this.activatedRoute.snapshot.paramMap.get('id');

    this.feedbackService.initializeData(this.routeId).then((content) => {
      loader.dismiss();
      this.contentList.next(content);
    });
  }

  submitFeedback() {
    console.log('Value of Feedback', this.feedback);
    this.feedbackService.storeFeedbackInDatabase(this.feedback);
    this.feedback = '';
  }
}
