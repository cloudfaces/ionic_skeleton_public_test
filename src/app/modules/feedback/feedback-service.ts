import { Injectable } from '@angular/core';
import { SideMenuService } from 'src/app/services/side-menu.service';
import { AppDataService } from 'src/app/services/app-data.service';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class FeedbackService {
  systemLanguage = new BehaviorSubject([]);

  constructor(
    private menuService: SideMenuService,
    private dataService: AppDataService
  ) {}

  async initializeData(routeId) {
    const databaseCollections = this.menuService.getdatabaseCollectoin(routeId);
    console.log('Database Collections', databaseCollections.collection);

    const tempItems = await this.dataService.getData(
      databaseCollections.collection
    );

    console.log('Feedback Data', tempItems.data);

    // get System Language Data from Data service
    this.systemLanguage.next(this.dataService.systemLanguages[0]);

    return [...tempItems.data];
  }

  storeFeedbackInDatabase(feedback) {
    const collectionName = 'feedback';
    this.dataService
      .getData(collectionName)
      .then(async (res: any) => {
        console.log('data', res);
        if (res.data.length) {
          // update a new Feedback in Directus Database
          const updateFeedback = await this.dataService.updateItem(
            collectionName,
            res.data[0].id,
            {
              feedback,
            }
          );
          console.log('Updated Feedback', updateFeedback);
        } else {
          // create/Add a new Feedback in Directus
          const addNewFeedback = await this.dataService.addNewItem(
            collectionName,
            {
              feedback,
            }
          );
          console.log('Added new Feedback', addNewFeedback);
        }
      })
      .catch((error) => {
        console.log(
          'Error while getting data inside push Notification Service',
          error
        );
      });
  }
}
