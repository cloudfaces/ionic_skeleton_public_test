import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { CategoryDetailService } from '../category-details-service';

@Component({
  selector: 'app-category-item-details',
  templateUrl: './category-item-details.component.html',
  styleUrls: ['./category-item-details.component.scss'],
})
export class CategoryItemDetailsComponent implements OnInit {
  contentObject = {};

  routeInfo = {
    url: '',
    direction: 'back',
  };

  constructor(
    private router: Router,
    private categoryDetailService: CategoryDetailService
  ) {}

  ngOnInit() {
    this.contentObject = this.router.getCurrentNavigation().extras.state;
    console.log('value of Content Object', this.contentObject);

    if (this.contentObject) {
      this.routeInfo.url = JSON.parse(JSON.stringify(this.contentObject)).url;
    }
  }
}
