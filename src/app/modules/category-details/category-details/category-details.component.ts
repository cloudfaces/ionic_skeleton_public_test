import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { CategoryDetailService } from '../category-details-service';
import { BehaviorSubject } from 'rxjs';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-category-details',
  templateUrl: './category-details.component.html',
  styleUrls: ['./category-details.component.scss'],
})
export class CategoryDetailsComponent implements OnInit {
  path = '';
  navigationExtras: NavigationExtras = {};
  routeId = '';

  contentList = new BehaviorSubject([]);

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private categoryDetailService: CategoryDetailService,
    private loadingCtrl: LoadingController
  ) {}

  async ngOnInit() {
    this.path = window.location.pathname;
    console.log('Path Inbox ', this.path);

    const loader = await this.loadingCtrl.create({
      message: 'Loading...',
    });

    await loader.present();

    this.routeId = this.activatedRoute.snapshot.paramMap.get('id');

    this.categoryDetailService.initializeData(this.routeId).then((content) => {
      loader.dismiss();
      this.contentList.next(content);
    });
  }
  openDetail(content) {
    this.navigationExtras.state = {
      url: this.path,
      routeId: this.routeId,
      ...content,
    };
    this.router.navigate(
      [`${this.path}/category-item-detail`],
      this.navigationExtras
    );
  }
}
