import { Injectable } from '@angular/core';
import { SideMenuService } from 'src/app/services/side-menu.service';
import { AppDataService } from 'src/app/services/app-data.service';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CategoryDetailService {
  systemLanguage = new BehaviorSubject([]);

  constructor(
    private menuService: SideMenuService,
    private dataService: AppDataService
  ) {}

  async initializeData(routeId) {
    const databaseCollections = this.menuService.getdatabaseCollectoin(routeId);
    const lengthOfObject = Object.keys(databaseCollections).length;
    console.log('Length of database Collection Object', lengthOfObject);
    console.log('Database Collections', databaseCollections.collection);

    const tempItems = await this.dataService.getData(
      databaseCollections.collection
    );

    console.log('value of temp Items', tempItems);

    // get System Language Data from Data service
    this.systemLanguage.next(this.dataService.systemLanguages[0]);

    return [...tempItems.data];
  }
}
