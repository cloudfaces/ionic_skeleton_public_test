import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CategoryDetailsComponent } from './category-details/category-details.component';
import { CategoryItemDetailsComponent } from './category-item-details/category-item-details.component';
import { RouterModule } from '@angular/router';
import { SystemLanguagesPipe } from 'src/app/pipes/system-languages.pipe';
import { HeaderComponent } from 'src/app/components/header/header.component';

const routes = [
  {
    path: '',
    component: CategoryDetailsComponent,
  },
  {
    path: 'category-item-detail',
    component: CategoryItemDetailsComponent,
  },
];

@NgModule({
  declarations: [
    CategoryDetailsComponent,
    CategoryItemDetailsComponent,
    SystemLanguagesPipe,
    HeaderComponent,
  ],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [
    CategoryDetailsComponent,
    CategoryItemDetailsComponent,
    SystemLanguagesPipe,
    HeaderComponent,
  ],
})
export class CategoryDetailsModule {}
