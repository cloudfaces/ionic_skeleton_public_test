import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { NotificationsService } from '../notifications-service';
import { BehaviorSubject } from 'rxjs';
import { SideMenuService } from 'src/app/services/side-menu.service';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss'],
})
export class NotificationsComponent implements OnInit {
  routeId = '';
  contentList = new BehaviorSubject([]);
  navigationExtras: NavigationExtras = {};
  path = '';

  constructor(
    private loadingCtrl: LoadingController,
    private activatedRoute: ActivatedRoute,
    private notificationsService: NotificationsService,
    private router: Router,
    private menuService: SideMenuService
  ) {}

  async ngOnInit() {
    this.path = window.location.pathname;
    console.log('Path Notifications ', this.path);

    const loader = await this.loadingCtrl.create({
      message: 'Loading...',
    });

    await loader.present();

    this.routeId = this.activatedRoute.snapshot.paramMap.get('id');

    this.notificationsService
      .initializeData(this.routeId)
      .then((content) => {
        loader.dismiss();
        this.contentList.next(content);
      })
      .catch((error) => {
        loader.dismiss();
        console.log('Error While loading data', error);
      });
  }

  openNotificationDetail(content) {
    console.log('Value of content inside Open NotificationDetails', content);
    if (content.action_direct_forward) {
      const actionPageUID = content.action_page.split('/')[1];
      // get actionPage Index and udpate side menu index
      this.menuService.selectedIndex = this.menuService.appPages
        .getValue()
        .findIndex((sMenu) => sMenu.uid === actionPageUID);

      this.router.navigate([content?.action_page], { replaceUrl: true });
    } else {
      this.navigationExtras.state = {
        url: this.path,
        routeId: this.routeId,
        ...content,
      };

      this.router.navigate(
        [`${this.path}/notification-detail`],
        this.navigationExtras
      );
    }
  }
}
