import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotificationsComponent } from './notifications/notifications.component';
import { RouterModule } from '@angular/router';
import { HeaderComponent } from 'src/app/components/header/header.component';
import { SystemLanguagesPipe } from 'src/app/pipes/system-languages.pipe';
import { NotificationDetailsComponent } from './notification-details/notification-details.component';

const routes = [
  {
    path: '',
    component: NotificationsComponent,
  },
  {
    path: 'notification-detail',
    component: NotificationDetailsComponent,
  },
];

@NgModule({
  declarations: [
    NotificationsComponent,
    NotificationDetailsComponent,
    HeaderComponent,
    SystemLanguagesPipe,
  ],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [
    NotificationsComponent,
    NotificationDetailsComponent,
    HeaderComponent,
  ],
})
export class NotificationsModule {}
