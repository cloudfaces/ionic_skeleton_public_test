import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { SideMenuService } from 'src/app/services/side-menu.service';

@Component({
  selector: 'app-notification-details',
  templateUrl: './notification-details.component.html',
  styleUrls: ['./notification-details.component.scss'],
})
export class NotificationDetailsComponent implements OnInit {
  pathToParent = '';

  contentList: NavigationExtras = {};
  routeInfo = {
    url: '',
    direction: 'back',
  };

  constructor(private router: Router, private menuService: SideMenuService) {}

  ngOnInit() {
    this.contentList = this.router.getCurrentNavigation().extras.state;

    if (this.contentList) {
      this.routeInfo.url = JSON.parse(JSON.stringify(this.contentList)).url;
    }
  }

  goToURL(content) {
    const actionPageUID = content.action_page.split('/')[1];
    // get actionPage Index and udpate side menu index
    this.menuService.selectedIndex = this.menuService.appPages
      .getValue()
      .findIndex((sMenu) => sMenu.uid === actionPageUID);

    this.router.navigate([content?.action_page], { replaceUrl: true });
  }
}
