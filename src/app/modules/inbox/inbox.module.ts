import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InboxComponent } from './inbox/inbox.component';
import { RouterModule } from '@angular/router';
import { SystemLanguagesPipe } from 'src/app/pipes/system-languages.pipe';
import { HeaderComponent } from 'src/app/components/header/header.component';

const routes = [
  {
    path: '',
    component: InboxComponent,
  },
];

@NgModule({
  declarations: [InboxComponent, SystemLanguagesPipe, HeaderComponent],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [InboxComponent, SystemLanguagesPipe, HeaderComponent],
})
export class InboxModule {}
