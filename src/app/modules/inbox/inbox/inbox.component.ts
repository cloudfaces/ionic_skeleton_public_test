import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { InboxService } from '../inbox-service';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.scss'],
})
export class InboxComponent implements OnInit {
  contentList = new BehaviorSubject([]);
  title: '';
  user: any;
  routeId = '';

  constructor(
    private activatedRoute: ActivatedRoute,
    private inboxService: InboxService,
    private loadingCtrl: LoadingController
  ) {}

  async ngOnInit() {
    const path = window.location.pathname;
    console.log('Path Inbox ', path);

    const loader = await this.loadingCtrl.create({
      message: 'Loading...',
    });

    await loader.present();

    this.routeId = this.activatedRoute.snapshot.paramMap.get('id');

    this.inboxService.initializeData(this.routeId).then((content) => {
      loader.dismiss();
      this.contentList.next(content);
    });
  }

  toggleReadMore(e: any) {
    const parentBtn = e.target.parentElement;
    const descEl = e.target.parentElement.previousElementSibling;
    descEl.classList.toggle('open');
    parentBtn.classList.toggle('openText');
  }
}
