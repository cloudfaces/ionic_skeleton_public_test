import { Injectable } from '@angular/core';
import { AppDataService } from './app-data.service';
import { BehaviorSubject } from 'rxjs';
import { PushNotificationService } from './push-notifications.service';
import { Capacitor } from '@capacitor/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class SideMenuService {
  public appPages = new BehaviorSubject([]);
  public selectedIndex = 0;

  currentLoggedInUserRole = '';

  constructor(
    private dataService: AppDataService,
    private pushNotificationService: PushNotificationService
  ) {}

  async initSideMenu() {
    const tempSideMenu = await this.dataService.getData('slideout_menu');

    const sideMenuData = [];

    console.log('Temp Side Menu Data', tempSideMenu);

    tempSideMenu.data.map((sd) => {
      const tempSideMenuData = JSON.parse(JSON.stringify(sd));
      const tempObj = {
        ['title']: tempSideMenuData.translations[0].title,
        ['url']: tempSideMenuData.path
          ? tempSideMenuData.path
          : tempSideMenuData.module.default_path,
        ['icon']: tempSideMenuData.icon,
        ['uid']: tempSideMenuData.module.uid,
        ['collections']: tempSideMenuData.module.collections,
        ['require_login']: tempSideMenuData.require_login,
        ['require_role']: tempSideMenuData.require_role,
        ['visible']: tempSideMenuData.visible,
        ['layout']: tempSideMenuData.module.settings
          ? tempSideMenuData.module.settings.layout
          : '',
      };
      sideMenuData.push(tempObj);
    });
    console.log('Value of Side out menu', sideMenuData);

    // this.appPages.next([...tempSideMenu.data]);
    this.appPages.next([...sideMenuData]);
    // const sortedSideMenu: any = tempSideMenu.data.sort(
    //   (a: any, b: any) => a.order - b.order
    // )[0];

    // Init Push Notfications
    const isPushNotificationAvailable = Capacitor.isPluginAvailable(
      'PushNotifications'
    );
    console.log('Is Push Notification Avaialble', isPushNotificationAvailable);
    if (isPushNotificationAvailable) {
      this.pushNotificationService.initPushNotification('');
    }

    // Init System Languages Data
    const systemLanguageSnap = await this.dataService.getSystemLanguagesData(
      'system_languages'
    );

    // return sortedSideMenu?.url;
    return sideMenuData[0]?.url;
  }

  getdatabaseCollectoin(id) {
    const temp = this.findSelectedSidemenu(id);
    return temp.collections;
  }

  getLayoutFromDatabase(id) {
    const temp = this.findSelectedSidemenu(id);
    return temp.layout;
  }
  findSelectedSidemenu(id) {
    return this.appPages.getValue().find((obj) => obj.uid === id);
  }
}
