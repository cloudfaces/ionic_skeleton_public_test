import { Injectable, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import * as Color from 'color';

@Injectable({
  providedIn: 'root',
})
export class ThemeService {
  constructor(@Inject(DOCUMENT) private document: Document) {}

  setTheme(theme) {
    const cssText = this.CSSTextGenerator(theme);
    this.setGlobalCSS(cssText);
  }

  // Define a single CSS variable
  setVariable(name, value) {
    this.document.documentElement.style.setProperty(name, value);
  }

  setGlobalCSS(css: string) {
    this.document.documentElement.style.cssText = css;
  }

  CSSTextGenerator(colors) {
    const defaults = {
      primary: '#3880ff',
      secondary: '#0cd1e8',
      tertiary: '#7044ff',
      success: '#10dc60',
      warning: '#ffce00',
      danger: '#f04141',
      dark: '#222428',
      medium: '#989aa2',
      light: '#f4f5f8',
    };

    colors = { ...defaults, ...colors };

    const {
      primary,
      secondary,
      tertiary,
      success,
      warning,
      danger,
      dark,
      medium,
      light,
    } = colors;

    const shadeRatio = 0.1;
    const tintRatio = 0.1;

    return `
    --ion-color-base: ${light};
    --ion-color-contrast: ${dark};
    --ion-background-color: ${light};
    --ion-text-color: ${dark};
    --ion-toolbar-background-color: ${this.contrast(light, 0.1)};
    --ion-toolbar-text-color: ${this.contrast(dark, 0.1)};
    --ion-item-background-color: ${this.contrast(light, 0.3)};
    --ion-item-text-color: ${this.contrast(dark, 0.3)};

    --ion-color-primary: ${primary};
    --ion-color-primary-rgb: ${this.colorToRGB(primary)};
    --ion-color-primary-contrast: ${this.contrast(primary)};
    --ion-color-primary-contrast-rgb: 255, 255, 255;
    --ion-color-primary-shade:  ${Color(primary).darken(shadeRatio)};
    --ion-color-primary-tint:  ${Color(primary).lighten(tintRatio)};

    --ion-color-secondary: ${secondary};
    --ion-color-secondary-rgb: ${this.colorToRGB(secondary)};
    --ion-color-secondary-contrast: ${this.contrast(secondary)};
    --ion-color-secondary-contrast-rgb: 255, 255, 255;
    --ion-color-secondary-shade:  ${Color(secondary).darken(shadeRatio)};
    --ion-color-secondary-tint: ${Color(secondary).lighten(tintRatio)};

    --ion-color-tertiary:  ${tertiary};
    --ion-color-tertiary-rgb: ${this.colorToRGB(tertiary)};;
    --ion-color-tertiary-contrast: ${this.contrast(tertiary)};
    --ion-color-tertiary-contrast-rgb: 255, 255, 255;
    --ion-color-tertiary-shade: ${Color(tertiary).darken(shadeRatio)};
    --ion-color-tertiary-tint:  ${Color(tertiary).lighten(tintRatio)};

    --ion-color-success: ${success};
    --ion-color-success-rgb: ${this.colorToRGB(success)};;
    --ion-color-success-contrast: ${this.contrast(success)};
    --ion-color-success-contrast-rgb: 255, 255, 255;
    --ion-color-success-shade: ${Color(success).darken(shadeRatio)};
    --ion-color-success-tint: ${Color(success).lighten(tintRatio)};

    --ion-color-warning: ${warning};
    --ion-color-warning-rgb: ${this.colorToRGB(warning)};;
    --ion-color-warning-contrast: ${this.contrast(warning)};
    --ion-color-warning-contrast-rgb: 255, 255, 255;
    --ion-color-warning-shade: ${Color(warning).darken(shadeRatio)};
    --ion-color-warning-tint: ${Color(warning).lighten(tintRatio)};

    --ion-color-danger: ${danger};
    --ion-color-danger-rgb: ${this.colorToRGB(danger)};;
    --ion-color-danger-contrast: ${this.contrast(danger)};
    --ion-color-danger-contrast-rgb: 255, 255, 255;
    --ion-color-danger-shade: ${Color(danger).darken(shadeRatio)};
    --ion-color-danger-tint: ${Color(danger).lighten(tintRatio)};

    --ion-color-dark: ${dark};
    --ion-color-dark-rgb: ${this.colorToRGB(dark)};;
    --ion-color-dark-contrast: ${this.contrast(dark)};
    --ion-color-dark-contrast-rgb: 255, 255, 255;
    --ion-color-dark-shade: ${Color(dark).darken(shadeRatio)};
    --ion-color-dark-tint: ${Color(dark).lighten(tintRatio)};

    --ion-color-medium: ${medium};
    --ion-color-medium-rgb: ${this.colorToRGB(medium)};;
    --ion-color-medium-contrast: ${this.contrast(medium)};
    --ion-color-medium-contrast-rgb: 255, 255, 255;
    --ion-color-medium-shade: ${Color(medium).darken(shadeRatio)};
    --ion-color-medium-tint: ${Color(medium).lighten(tintRatio)};

    --ion-color-light: ${light};
    --ion-color-light-rgb: ${this.colorToRGB(light)};;
    --ion-color-light-contrast: $${this.contrast(light)};
    --ion-color-light-contrast-rgb: 0, 0, 0;
    --ion-color-light-shade: ${Color(light).darken(shadeRatio)};
    --ion-color-light-tint: ${Color(light).lighten(tintRatio)};
    `;
  }

  contrast(color, ratio = 0.8) {
    color = Color(color);
    return color.isDark() ? color.lighten(ratio) : color.darken(ratio);
  }

  colorToRGB(color) {
    let r = '';
    let g = '';
    let b = '';

    // 3 digits
    if (color.length === 4) {
      r = '0x' + color[1] + color[1];
      g = '0x' + color[2] + color[2];
      b = '0x' + color[3] + color[3];

      // 6 digits
    } else if (color.length === 7) {
      r = '0x' + color[1] + color[2];
      g = '0x' + color[3] + color[4];
      b = '0x' + color[5] + color[6];
    }

    const temp = 'rgb(' + +r + ',' + +g + ',' + +b + ')';
    // console.log(`color = ${color} and rgb = ${temp}`);
    return temp;
  }
}
