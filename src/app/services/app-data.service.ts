import { Injectable } from '@angular/core';
import { DirectusService } from './directus.service';
import { LoadingController } from '@ionic/angular';
import { BehaviorSubject, from } from 'rxjs';
import { map } from 'rxjs/internal/operators/map';

@Injectable({
  providedIn: 'root',
})
export class AppDataService {
  currentLoading = null;

  public systemLanguages = [];

  constructor(
    private directusService: DirectusService,
    private loadingCtrl: LoadingController
  ) {}

  getUser() {
    return this.directusService.api.getUser(15);
  }

  geCurrentLoggedIntUserRole() {
    return from(this.directusService.api.getMe({ fields: '*.*' }))
      .pipe(
        map((userInfo) => {
          return userInfo.data.role.name;
        })
      )
      .toPromise();
  }

  getAllCollections() {
    return this.directusService.api.getCollections();
  }

  getData(collectionName) {
    return this.directusService.api.getItems(collectionName, { fields: '*.*' });
  }

  getCollectionData(collectionName) {
    return this.directusService.api.getCollection(collectionName);
  }

  async presentLoader(options: object) {
    // Dismiss previously created loading
    if (this.currentLoading != null) {
      this.currentLoading.dismiss();
    }

    this.currentLoading = await this.loadingCtrl.create(options);

    return await this.currentLoading.present();
  }

  async dismissLoader() {
    console.log('Outside If inside dismiss loader');
    if (this.currentLoading != null) {
      console.log('Inside if inside dismiss loader');
      await this.loadingCtrl.dismiss();
      this.currentLoading = null;
    }
    return;
  }

  addNewItem(collection, body) {
    return this.directusService.api.createItem(collection, body);
  }
  updateItem(collection, primaryKey, data) {
    return this.directusService.api.updateItem(collection, primaryKey, data);
  }

  async getSystemLanguagesData(collection) {
    const temp = await this.getData(collection);
    this.systemLanguages.push([...temp.data]);
  }
}
