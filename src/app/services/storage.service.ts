import { Injectable } from '@angular/core';
import { Plugins } from '@capacitor/core';
const { Storage } = Plugins;

@Injectable({
  providedIn: 'root',
})
export class StorageService {
  constructor() {}

  async storeObject(storeObject) {
    await Storage.set(storeObject);
  }

  // JSON "get" example  { key: 'user' }
  async getObject(key) {
    const ret = await Storage.get(key);
    const user = JSON.parse(ret.value);
  }

  async setItem(item) {
    await Storage.set(item);
  }

  // { key: 'name' }
  async getItemByKey(key) {
    const { value } = await Storage.get(key);
    console.log('Got item: ', value);

    return value;
  }

  // { key: 'name' }
  async removeItemByKey(key) {
    await Storage.remove(key);
  }

  async keys() {
    const { keys } = await Storage.keys();
    console.log('Got keys: ', keys);
  }

  async clear() {
    await Storage.clear();
  }
}
