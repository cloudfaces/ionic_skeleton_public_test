import { Injectable } from '@angular/core';
import { DirectusService } from './directus.service';
import { ILoginCredentials } from '@directus/sdk-js/dist/types/schemes/auth/Login';
import { databaseSettings } from 'src/environments/database-settings';
import { Storage } from '@ionic/storage';
import { SideMenuService } from './side-menu.service';
import { AppDataService } from './app-data.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  isLoggedIn = false;
  creds;

  constructor(
    private directusService: DirectusService,
    private storage: Storage,
    private menuService: SideMenuService,
    private dataService: AppDataService,
    private router: Router
  ) {}

  loginUser(email, password) {
    const data: ILoginCredentials = {
      url: databaseSettings.directus.projectUrl,
      project: databaseSettings.directus.projectId,
      email,
      password,
      persist: true,
    };

    this.isLoggedIn = true;
    // return this.directusService.api.login(data);
    return this.directusService.api.login(data).then((user) => {
      // set Email and password in local storage
      this.storage.set('creds', { email, password });

      // init Side Menu
      this.menuService.initSideMenu().then((url) => {
        // this.menuService.appPages.subscribe((menuData) => {
        // Dismiss the loader
        this.dataService.dismissLoader();

        // this.router.navigate([`${menuData[0].url}`], { replaceUrl: true });
        this.router.navigate([`${url}`], { replaceUrl: true });
      });
    });
  }

  logoutUser() {
    this.isLoggedIn = false;
    this.storage.remove('creds');
    return this.directusService.api.logout();
  }

  async getLocalStorage() {
    this.creds = await this.storage.get('creds');

    if (this.creds) {
      console.log('Email and password exists');

      this.dataService.presentLoader({
        message: 'Signing In...',
      });

      this.loginUser(this.creds.email, this.creds.password)
        .then(() => {
          // this.dataService.dismissLoader();
        })
        .catch((error) => {
          this.dataService.dismissLoader();
          console.log('Error while logging in from auth service', error);
        });
    }
  }
}
