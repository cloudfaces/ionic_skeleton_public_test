import { Injectable } from '@angular/core';
import DirectusSDK from '@directus/sdk-js';
import { databaseSettings } from 'src/environments/database-settings';

@Injectable({
  providedIn: 'root',
})
export class DirectusService {
  private internalSDKClient = new DirectusSDK({
    url: databaseSettings.directus.projectUrl,
    project: databaseSettings.directus.projectId,
  } as any);

  public get api(): DirectusSDK {
    return this.internalSDKClient;
  }
}
