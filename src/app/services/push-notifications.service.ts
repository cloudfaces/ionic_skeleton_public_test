import { Injectable } from '@angular/core';
import {
  Plugins,
  PushNotification,
  PushNotificationToken,
  PushNotificationActionPerformed,
} from '@capacitor/core';
import { AppDataService } from './app-data.service';

const { PushNotifications } = Plugins;

@Injectable({
  providedIn: 'root',
})
export class PushNotificationService {
  constructor(private dataService: AppDataService) {}

  initPushNotification(user) {
    // Request permission to use push notificaiton
    // iOS will promt user and return if they granted Permission or not
    // Android will just grant without promting

    PushNotifications.requestPermission().then((result) => {
      if (result.granted) {
        // Register with Apple/Google to receive push via APNS/FCM
        PushNotifications.register();
      } else {
        // show some error

        console.log('Permission Rejected', result);
      }
    });

    // On success, we should be able to receive notification
    PushNotifications.addListener(
      'registration',
      (token: PushNotificationToken) => {
        console.log('Push registration success, token: ' + token.value);

        if (token.value) {
          this.updateDeviceTokenInDirectus(token.value);
        }
      }
    );

    // Some issue with our setup and push will not work
    PushNotifications.addListener('registrationError', (error: any) => {
      console.log('Error on Registration: ', JSON.stringify(error));
    });

    // Show us the notification payload if the app is open on our device.
    PushNotifications.addListener(
      'pushNotificationReceived',
      async (notification: PushNotification) => {
        // await Toast.show({
        //   text
        // })
        console.log('Push Received: ', JSON.stringify(notification));
      }
    );

    // Method called when tapping on the notification
    PushNotifications.addListener(
      'pushNotificationActionPerformed',
      (notification: PushNotificationActionPerformed) => {
        console.log('Push action performed', notification);
      }
    );
  }

  updateDeviceTokenInDirectus(token) {
    const collectionName = 'device_token_notification';
    this.dataService
      .getData(collectionName)
      .then(async (res: any) => {
        console.log('data', res);
        if (res.data.length) {
          // update a new Token in Directus Database
          const updateToken = await this.dataService.updateItem(
            collectionName,
            res.data[0].id,
            {
              device_token: token,
            }
          );
          console.log('Updated Token', updateToken);
        } else {
          // create/Add a new token in Directus
          const addNewToken = await this.dataService.addNewItem(
            'device_token_notification',
            {
              device_token: token,
            }
          );
          console.log('Added new Token', addNewToken);
        }
      })
      .catch((error) => {
        console.log(
          'Error while getting data inside push Notification Service',
          error
        );
      });
  }
}
