import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { SideMenuService } from 'src/app/services/side-menu.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  @Input() showButton = {};
  @Input() routeInfo: any;
  @Input() isLoginPage: boolean;

  constructor(private router: Router, private menuService: SideMenuService) {}

  ngOnInit() {
    console.log(
      `Show Button =', ${this.showButton}. RoutingInfo = ${this.routeInfo}, IsLoginPage = ${this.isLoginPage} `
    );
  }

  openNotificationPage() {
    this.menuService.selectedIndex = this.menuService.appPages
      .getValue()
      .findIndex((sMenu) => sMenu.uid === 'Notifications');
    this.router.navigate(['notifications/Notifications'], { replaceUrl: true });
  }
}
