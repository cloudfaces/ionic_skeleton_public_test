import { Component, OnInit, AfterViewInit } from '@angular/core';
import { SideMenuService } from 'src/app/services/side-menu.service';
import { Router, NavigationExtras } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Plugins } from '@capacitor/core';
import { AppService } from 'src/app/app.service';

const { Device } = Plugins;

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss'],
})
export class SideMenuComponent implements OnInit {
  appVersion = '';

  navigationExtras: NavigationExtras = {};

  appPages = new BehaviorSubject([]);

  isLoginOnStart = false;
  // firstRoute = '';

  constructor(
    public menu: SideMenuService,
    private router: Router,
    private authService: AuthenticationService,
    private appService: AppService
  ) {
    this.menu.initSideMenu();
  }

  async ngOnInit() {
    setTimeout(async () => {
      this.isLoginOnStart = this.appService.systemSettings.login_on_start;
      const firstRoute = this.isLoginOnStart
        ? 'login'
        : this.menu.appPages.getValue()[0].url;

      console.log('value of firstRoute', firstRoute);

      this.router.navigate([firstRoute], { replaceUrl: true });
    }, 2000);

    const deviceInfo = await Device.getInfo();
    this.appVersion = deviceInfo.appVersion;
    console.log('Device Info', deviceInfo.appVersion);
  }

  navigateToModule(moduleData, index) {
    this.menu.selectedIndex = index;
    console.log('Module data', moduleData);

    this.navigationExtras.state = moduleData.collections;
    this.navigationExtras.replaceUrl = true;

    this.router.navigate([`/${moduleData.url}`], this.navigationExtras);
  }

  logout() {
    console.log('Logout Clicked');
    this.authService
      .logoutUser()
      .then(() => {
        this.router.navigate(['/login'], { replaceUrl: true });
      })
      .catch((error) => {
        console.log('Error While Logging out the user', error);
      });
  }

  isMenuAvailable(menuData) {
    if (this.isLoginOnStart) {
      // get Current Logged In user Info
      // this.currentLoggedInUserRole = await this.dataService.geCurrentLoggedIntUserRole();

      return menuData.visible;
    } else {
      return menuData.visible && menuData.require_login === this.isLoginOnStart;
    }
  }
}
