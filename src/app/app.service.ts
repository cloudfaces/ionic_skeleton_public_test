import { Injectable } from '@angular/core';
import { AppDataService } from './services/app-data.service';
import { ThemeService } from './services/theme.service';
import { BehaviorSubject } from 'rxjs';
import { AuthenticationService } from './services/authentication.service';

@Injectable({
  providedIn: 'root',
})
export class AppService {
  public sytstemSettingData = new BehaviorSubject<any>({});

  constructor(
    private dataService: AppDataService,
    private themeService: ThemeService,
    private authService: AuthenticationService
  ) {}

  async initAppData() {
    const tempSystemSettingData = await this.dataService.getData(
      'system_settings'
    );

    this.sytstemSettingData.next(
      JSON.parse(JSON.stringify(tempSystemSettingData)).data[0]
    );

    if (this.systemSettings.login_on_start) {
      // Init and Get Local Storage data
      this.authService.getLocalStorage();
    }

    console.log('System Setting Data', this.systemSettings);
    console.log('Theme Color Object', this.systemSettings.theme_color);
    this.themeService.setTheme(this.systemSettings.theme_color);
  }

  get systemSettings() {
    return this.sytstemSettingData.getValue();
  }

  canShowSideMenu() {
    return this.systemSettings.login_on_start
      ? this.authService?.isLoggedIn
      : true;
  }
}
