# README

This README would normally document whatever steps are necessary to get your application up and running.

## Init App:

- Copy/clone the project.
- Go to the "capacitor.config.json" and change AppId and AppName and save the file.
- paste "icon.png" and "splash.png" in resources folder. icon image should ideally be
  at least 1024Ã—1024px and splash image should ideally be at least 2732Ã—2732px. The splash screen's artwork should roughly fit within a square (1200Ã—1200px) at the center of the image.
- open "AppRoot\src\environments\database-settings" and add directus projectUrl and
  projectId in "directus" object.
- If you need push notifications in the project then firstly, register the apps in
  firestore database. Get Firebase configuration code and paste it in the "firebase" object of " AppRoot\src\environments\database-settings".
  Also download both "google-service.json" and "GoogleService-Info.plist" files and paste them in resources folder.
- Now run the following command.
  npm run init-app
  it will take a while and setup whole project.

## Copy And Paste GoogleService file in Appropriate Platform:

In Order to Push notifications Works perfectly on both platforms. We have to put "google-service.json" and "GoogleService-Info.plist" files in their required positions.

### For IOS

- Copy the "GoogleService-info.plist" file from resources folder.
- Go to the : "ios/App/App/".
- Paste the file there.

### For Android

- Copy the "google-service.json" file from resources folder.
- Go to the : "android/app/".
- Paste the file there.

## Push code to the repo configured in the AppFlow.

## Change Bundle version from Editor (ex: vs Code):

Every time you push a new binary to both platform's store, You have to change the bundle version of your apk/ipa file. Otherwise they will get rejected.

### For IOS

- Open the "info.plist" file.
- You can find it under : "ios/App/App/info.plist"
- Change the "CFBundleVersion"
- Save the File and push the code

### For Android

- Open the "build.gradle" file.
- You can find it under: "android/app/build.gradle"
- Change the "versionCode" which is a number
- Change the "versionName" which is a string value
